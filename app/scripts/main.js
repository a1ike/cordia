$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.c-modal').toggle();
  });

  $('.c-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'c-modal__centered') {
      $('.c-modal').hide();
    }
  });

  $('.c-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.c-modal').hide();
  });

  new Swiper('.c-about__cards', {
    navigation: {
      nextEl: '.c-about__cards .swiper-button-next',
      prevEl: '.c-about__cards .swiper-button-prev',
    },
    spaceBetween: 100,
    loop: true,
    /* autoplay: {
      delay: 5000,
    }, */
  });

  new Swiper('.c-reviews__cards', {
    navigation: {
      nextEl: '.c-reviews__cards .swiper-button-next',
      prevEl: '.c-reviews__cards .swiper-button-prev',
    },
    spaceBetween: 1000,
    loop: true,
    /* autoplay: {
      delay: 5000,
    }, */
  });

  $(window).on('load resize', function () {
    if ($(window).width() < 1200) {
      $('.c-qa__tab').removeClass('current');
      $('.c-qa__content').removeClass('current');

      $('.c-qa__tab-mob').on('click', function (e) {
        e.preventDefault();

        $(this).next().slideToggle('fast');
      });
    } else {
      $('.c-qa__tab').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('.c-qa__tab').removeClass('current');
        $('.c-qa__content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
      });
    }
  });
});
